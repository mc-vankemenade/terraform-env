data "xenorchestra_pool" "xen-manage-pool" {
    name_label = "xen-host1"
}

data "xenorchestra_pool" "xen-worker-pool" {
    name_label = "xen-worker1"
}

data "xenorchestra_sr" "xen-manage-sr" {
    name_label = "Local storage"
    pool_id = data.xenorchestra-pool.xen-worker-pool.id
}

data "xenorchestra_sr" "xen-worker-sr" {
    name_label = "Local storage"
    pool_id = data.xenorchestra-pool.xen-manage-pool.id
}
