terraform {
    required_providers {
        xenorchestra = {
            source = "terra-farm/xenorchestra"
        }
    }
}

provider "xenorchestra" {
    url         = "ws://xen-management"
    username    = "admin@admin.net"
    password    = "admin"
}
