# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/terra-farm/xenorchestra" {
  version = "0.24.0"
  hashes = [
    "h1:ggfOjEPypvT24z7U36N1ZDlqwUdFI/6soZHTh+SPo2k=",
    "zh:30778042769b3668f6080d2335fe0730508e6d1ad9a1fbb60594ca3e03a4f8e8",
    "zh:53cdd86ce4b1e1c2ea5e2e4fca172fe971c847ce9c8912d93c09a0fab43fdc6f",
    "zh:58e5df7f14ea49fd35bba3bc96f4a106d2b3e1bf08d14de69f9e93504365de8a",
    "zh:9b8a314d575bf20c25ae9c4be087dcaf9f1afc4f2b0150af7d2695b2e26230a4",
    "zh:b52e2d3f71786adab345efeb776a4c4266a62a03621978851f4dd08f4a147c50",
    "zh:eafe0bda8093a9a68f7afcd03a6d03018160eb05497acbb3c3bcc187a0ff9996",
    "zh:f8df2e286feb8542df46c8c47ee00860b41fbb6f6fc6b216ac227e90bfd14c2f",
  ]
}
